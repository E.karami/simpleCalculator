var buttons = document.getElementsByTagName('td');
var input = document.getElementsByTagName('input')[0];
// var forbidden = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "-", "*", "/", "=", "."];
var forbiddenChar = ["+", "-", "*", "/", "=", "."];
for (var counter = 0; counter < buttons.length; counter++) {
    if (buttons[counter].innerText != '=') {
        buttons[counter].addEventListener('click', function() {
            addChar(this);
        });
    } else {
        buttons[counter].addEventListener('click', function() {
            showReslt(this);
        })
    }
}

function addChar(button) {
    if (input.value.length == 20) {
        return;
    }
    if (button.innerText == '-' || button.innerText == '+' || button.innerText == '*' || button.innerText == '/' ||
        button.innerText == '.') {
        if (button.innerText == '-' && input.value.charAt(input.value.length - 1) != '-') {
            input.value += button.innerText;
        }
        if (forbiddenChar.includes(input.value.charAt(input.value.length - 1)) || (input.value.length == 0 && button.innerText != '-')) {
            return;
        }
        if (button.innerText == '.') {
            var dot = input.value.lastIndexOf('.') + 0;
            var plus = input.value.lastIndexOf('+') + 0;
            var minus = input.value.lastIndexOf('-') + 0;
            var mult = input.value.lastIndexOf('*') + 0;
            var division = input.value.lastIndexOf('/') + 0;
            if (plus == undefined) {
                plus = 0;
            }
            if (dot == undefined) {
                dot = 0;
            }
            if (dot <= plus || dot <= minus || dot <= division || dot <= mult) {
                input.value += button.innerText;
                return;
            } else {
                return;
            }
        }
        input.value += button.innerText;
    } else {
        // if (button.innerText == "0") {
        //     if (input.value.length == 0 || (forbiddenChar.includes(input.value.charAt(input.value.length - 1)) && input.value.charAt(input.value.length - 1) !== '.')) {
        //         return;
        //     }
        // }
        input.value += button.innerText;
    }
}

function showReslt(button) {
    var flag = false;
    var result = [];
    var char = [];
    var text = '';
    var inp = input.value.substring(0, input.value.length);
    if (inp.charAt(0) == '-' && input.value.length != 1) {

        flag = true;
        inp = inp.substring(1, input.value.length)
    }
    for (var counter = 0; counter < inp.length; counter++) {
        if (inp.charAt(counter) == '-' && (inp.charAt(counter - 1) == '+' || inp.charAt(counter - 1) == '*' || inp.charAt(counter - 1) == '/')) {
            flag = true;
            counter++;
        }
        while ((!forbiddenChar.includes(inp.charAt(counter)) || inp.charAt(counter) == '.') && counter < inp.length) {
            text += inp.charAt(counter);
            counter++;
        }
        console.log(text);
        if (flag == true) {
            result.push(-Number(text));
            flag = false;
        } else {
            result.push(Number(text));
        }
        if (counter == inp.length) {
            break;
        }
        char.push(inp.charAt(counter));
        text = '';
    }
    if (result.length == char.length) {
        char = char.slice(0, char.length - 1);
    }
    console.log(result);
    console.log(char);
    for (var counter = 0; counter < char.length; counter++) {
        debugger;
        if (char[counter] == "*" || char[counter] == "/") {
            if (char[counter] == "*") {
                result[counter] = result[counter] * result[counter + 1];
            }
            if (char[counter] == "/") {
                result[counter] = result[counter] / result[counter + 1];
            }
            result = Array().concat(result.slice(0, counter + 1), result.slice(counter + 2, result.length));
            char = Array().concat(char.slice(0, counter), char.slice(counter + 1, char.length));
            console.log(result);
            console.log(char);
            counter--;
        }
    }
    for (var counter = 0; counter < char.length; counter++) {
        if (char[counter] == "+" || char[counter] == "-") {
            if (char[counter] == "+") {
                result[counter] = result[counter] + result[counter + 1];
            }
            if (char[counter] == "-") {
                result[counter] = result[counter] - result[counter + 1];
            }
            result = Array().concat(result.slice(0, counter + 1), result.slice(counter + 2, result.length));
            char = Array().concat(char.slice(0, counter), char.slice(counter + 1, char.length));
            console.log(result);
            console.log(char);
            counter--;
        }
    }
    input.value = result[0];
}

function Clear() {
    input.value = null;
}

function backSpace() {
    input.value = input.value.substr(0, input.value.length - 1);
}